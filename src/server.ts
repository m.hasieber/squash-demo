import express from 'express'
const app = express()
const port = 3000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/login', (req, res) => {
  const name = req.query["name"]
  const password = req.query["pass"]

  if (password != "pass") {
    return res.status(401).send("No login!")
  }

  res.send(`Hello ${name}!`)
})

app.get('/logout', (req, res) => {
  res.status(418).send("byebye!")
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})